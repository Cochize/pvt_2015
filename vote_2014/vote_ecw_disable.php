<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Prix de l'Economie Sociale 2014 - Votes en ligne</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
<?php
	include("my_connection.inc.php");
	$ip = $_SERVER[REMOTE_ADDR];
?>
<div id="container3">
	<div class="titre_page"><img src="logo.jpg" alt="Prix de l'Economie Sociale" width="500" /></div>
	<h1><br/><b>Votez pour une Entreprise du 4 au 31 ao&ucirc;t 2014</b></h1>
	<h1 style="color:#dd5e3b;">Catégorie Entreprise Confirmée Wallonne</h1>

	<!-- --------------------------------  ECW  ---------------------------------------------- -->
	<br/>
	<?php
	if(isset($_POST['envoi'])){
		
		if($_POST['email'] != "" && $_POST['checkbox'] != "" ){
		/// on vérifie si l'email est déjà encodé dans la BDD
		$chercheServ = mysql_query("select * from participant_ecw");
		$ServArray = array ();
		 while($servicetab= mysql_fetch_array($chercheServ)) {
			$ServNom = $servicetab[3];
			$ServId = $servicetab[0];
			// array_unshift ($ServArray, $servicetab);
			array_push($ServArray,$ServNom);
		}

			$from = $_POST['email'];
			$valide = "n";
			$resultat = $_POST['checkbox'];
			
			// on reprendre le résultat du vote
			$sql2="select * from fiches where id='$resultat'";
			$rs2 = mysql_query($sql2) or die(mysql_error());
			$row2 = mysql_fetch_array($rs2);	
			$vote = $row2["nom"];

			// on regarde si l'email n'est pas déjà dans la base
			$sql3="select * from participant_ecw where email='$from'";
			$rs3 = mysql_query($sql3) or die(mysql_error());
			$djou = mysql_num_rows($rs3);

		if ($djou==0){
			
		//if (in_array($from, $ServArray)) {
		// vérif de l'e-mail
			$atom   = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';   // caractères autorisés avant l'arobase
			$domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)'; // caractères autorisés après l'arobase (nom de domaine)
								   
			$regex = '/^' . $atom . '+' .   // Une ou plusieurs fois les caractères autorisés avant l'arobase
			'(\.' . $atom . '+)*' .         // Suivis par zéro point ou plus
											// séparés par des caractères autorisés avant l'arobase
			'@' .                           // Suivis d'un arobase
			'(' . $domain . '{1,63}\.)+' .  // Suivis par 1 à 63 caractères autorisés pour le nom de domaine
											// séparés par des points
			$domain . '{2,63}$/i';          // Suivi de 2 à 63 caractères autorisés pour le nom de domaine

			// test de l'adresse e-mail
			if (preg_match($regex, $from)) {
			//script de réception des donnés, d'envoi par mail
				
				$nom = addslashes($_POST['nom']);	
				$entreprise = addslashes($_POST['entreprise']);
				$crypt = sha1($from);
				
				
				$sep = md5(uniqid());
				$pvt = "vote@prixdeleconomiesociale.be";
				 
		
					// fonction mail();

							   $to = $from;

							   // *** Laisser tel quel

							   $JOUR  = date("Y-m-d");
							   $HEURE = date("H:i");
							   $date_vote = $JOUR.$HEURE ;
							   $Subject = "Validation de votre vote - $JOUR $HEURE";

								$mail_Data = "";
								$mail_Data .= "<html> \n";
								$mail_Data .= "<head> \n";
								$mail_Data .= "<title> Validation de votre vote </title> \n";
								$mail_Data .= "</head> \n";
								$mail_Data .= "<body> \n";
								$mail_Data .= "&Eacute;tape 2/2 : Validation de votre vote<br><br> \n";
								$mail_Data .= "Votre nom : ".$nom." - Votre structure : ".$entreprise."<br> \n";
								$mail_Data .= "Entreprise Wallonne soutenue : ".$vote."<br><br> \n";
								$mail_Data .= "Cliquez maintenant sur ce lien afin de valider votre vote :<br> \n";
								$mail_Data .= "<a href='http://www.prixdeleconomiesociale.be/vote/valide_ecw.php?email=".$crypt."' target='_blank'>http://www.prixdeleconomiesociale.be/vote/valide_ecw.php?email=".$crypt."</a><br><br> \n";
								$mail_Data .= "<b>Le lien ci-dessus n'est pas cliquable?</b> <br> \n";
								$mail_Data .= "Copiez et collez cette url directement dans votre navigateur afin valider votre vote.<br><br> \n";
								$mail_Data .= "Merci pour votre participation !<br> \n";
								$mail_Data .= "</body> \n";
								$mail_Data .= "</HTML> \n";

							   $headers  = "MIME-Version: 1.0 \n";
							   $headers .= "Content-type: text/html; charset=iso-8859-1 \n";
							   $headers .= "From: $pvt  \n";
							   $headers .= "Disposition-Notification-To: $pvt  \n";
							   $headers .= "Cc: vote@prixdeleconomiesociale.be  \n";
							   $headers .= "Reply-to: \"Prix Economie Sociale\" <vote@prixdeleconomiesociale.be> \n";

							   // Message de Priorité haute
							   // -------------------------
							   $headers .= "X-Priority: 1  \n";
							   $headers .= "X-MSMail-Priority: High \n";

							   $CR_Mail = TRUE;

							   $CR_Mail = @mail ($to, $Subject, $mail_Data, $headers);
							 
							   if ($CR_Mail === FALSE)   echo " ### CR_Mail=$CR_Mail - Erreur envoi mail <br> \n";
							   else                      echo " *** CR_Mail=$CR_Mail - Mail envoyé<br> \n";  
		
						// fonction mail();
		

				$sql="insert into participant_ecw (ip, nom, entreprise, email, valide, resultat, date_vote) values ('$ip', '$nom', '$entreprise', '$from', '$valide', '$resultat', '$date_vote') ";
				mysql_query($sql) or die (mysql_error());
				
				
				echo "<p id='validevote'>Merci pour votre participation ! <br/><br/>
				<b>Il vous reste une étape pour valider votre vote !</b><br/><br/>
				Afin de valider votre vote, rendez-vous dans votre boîte de messagerie.<br/>Un mail vous y attend*!<br/>Cet email contient un lien de validation.<br/>Cliquez sur celui-ci afin de <b>VALIDER VOTRE VOTE</b>**.<br/><br/>Veillez à vérifier également votre dossier spam !</p><br/><br/>";
				$display='display:none;';
				} // Fin de la vérification d'email
			else {
				echo "<p id='echecvote'><b>Echec du vote !<br/>L'adresse email encod&eacute;e ($from) n&rsquo;est pas valide.</b><br/></p>";
				
			}

		} // fin de la vérification que l'email n'est pas déjà dans la BDD
		else {
			echo "<p id='echecvote'><b>Echec du vote !<br/>Vous avez d&eacute;j&agrave; vot&eacute; avec cette adresse.<br/>Merci de veiller &agrave; valider votre vote via votre bo&icirc;te email.</b></p>";
		}

		} // Fin de la vérification des champs obligatoires
		else {
				echo "<p id='echecvote'><b>Echec du vote !<br/> Merci de vérifier si vous avez encodé une adresse email valide !</b><br/></p>";
				$resultat = $_POST['checkbox'];
			}


	}	// Fin if envoi
		
	?>

	
	<div id="vote" style="<?php echo $display; ?>">
		<table id="result">
			<form name="contact_frm" action="vote_ecw.php" method="POST">
					<tr>
						<td colspan="2">
					<?php
						
						if(isset($_GET["id"])){
							$id = $_GET["id"];
							$sql="select * from fiches where id = $id";			
							$rs=mysql_query($sql) or die (mysql_error());				
							while ($row=mysql_fetch_assoc($rs)) {
								print "<br/><img style='float:left; border:1px solid #cccccc; margin-right: 20px;' src='uploads/" .$row["logo"]."' />";
								print "<div id='titre_entr'>Vous avez s&eacute;lectionn&eacute; l&rsquo;entreprise : <b><br/>".$row["nom"]."</b></div>";
								print "<input id='choixxrad' type='hidden' name='checkbox' value=" .$row["id"].">";
							}
						}
						elseif(isset($resultat)){
							$sql="select * from fiches where id = $resultat";			
							$rs=mysql_query($sql) or die (mysql_error());				
							while ($row=mysql_fetch_assoc($rs)) {
								print "<br/><img style='float:left; border:1px solid #cccccc; margin-right: 20px;' src='uploads/" .$row["logo"]."' />";
								print "<div id='titre_entr'>Vous avez s&eacute;lectionn&eacute; l&rsquo;entreprise : <b><br/>".$row["nom"]."</b></div>";
								print "<input id='choixxrad' type='hidden' name='checkbox' value=" .$row["id"].">";
							}
						}
						else{
							echo "";
						}

					?>
						</td>

				    </tr>
					<tr>
						<td colspan="2">
							<h2 style="color:#dd5e3b;">&Eacute;tape 1/2 : Encoder une adresse e-mail valide</h2>
						</td>
					</tr>
					<tr>
						<td colspan="2">Encodez ci-dessous une adresse e-mail valide et cliquez sur "<b>JE VOTE POUR CETTE ENTREPRISE</b>" afin de confirmer votre choix.<br/><br/>
						</td>
					</tr>
				    <tr>
						<td>Votre nom : </td><td><input type="text" size="30" name="nom" /></td>
				    </tr>
				    <tr>
						<td>Votre structure : </td><td><input type="text" size="30" name="entreprise" /></td>
				    </tr>
				    <tr>
						<td>Votre email <span style='color:red;'><b>*</b></span> :</td><td><input type="text" size="30" name="email" /> /!\ adresse email valide obligatoire pour validation du vote !</td>
					</tr>
					<tr>
						<td colspan="2">
							<p id="expl">
								<b>Important :</b> Afin de participer au vote, il est indispensable d&rsquo;encoder une <strong>adresse email valide<span style='color:red;'>*</span></strong>.<br/>
								Apr&egrave;s avoir cliqu&eacute; sur "JE VOTE POUR CETTE ENTREPRISE", vous recevrez un email de validation &agrave; l&rsquo;adresse encod&eacute;e. 
								Vous devez <strong style='color:red;'>cliquer sur le lien de validation contenu dans l'email pour de VALIDER votre vote</strong>.
								<br/><strong>Si vous ne validez pas votre vote, celui-ci ne sera pas comptabilis&eacute; !</strong>
								<br/><br/>
							</p>
						</td>
					</tr>
				    <tr>
						<td colspan="2">
							<input id="button" type="submit" name="envoi" value=" JE VOTE POUR CETTE ENTREPRISE " /><br/>
						</td>
					</tr>
			</form>
		</table>
	</div>    
	<br/><br/>
	<p style="text-align:center;">
		* L'arrivée de l'email peut prendre plusieurs minutes<br/>
		** Si vous ne voyez pas arriver notre email, <b style="color:#ea7b05;">vérifiez qu'il ne se trouve pas dans votre dossier spam</b>.<br/>
		** Adresse GMail ? Vérifiez les nouveaux dossiers créés par le tri automatique de la nouvelle boîte GMail.
	</p>
	
</div>
<div id="footer-bottom2">
<?php
	include("footer.php");
?>
</div>


</body>
</html>