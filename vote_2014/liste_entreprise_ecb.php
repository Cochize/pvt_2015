<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Prix de l'Economie Sociale 2014</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<link href='http://fonts.googleapis.com/css?family=Actor' rel='stylesheet' type='text/css'>

	<!-- Add jQuery library -->
	<script type="text/javascript" src="lib/jquery-1.7.2.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="source/jquery.fancybox.js?v=2.0.6"></script>
	<link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.0.6" media="screen" />

	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.2" />
	<script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.2"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.2" />
	<script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.2"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.0"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay opening speed and opacity
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedIn : 500,
						opacity : 0.95
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background-color' : '#eee'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
	<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
	</style>

</head>

<body>
<?php
include("my_connection.inc.php");

?>
<div id="container">


<div class="titre_page"><img src="logo.jpg" alt="Prix de l'Economie Sociale" width="500" /></div>


<h1><br/><b>Votez pour une Entreprise du 4 au 31 ao&ucirc;t 2014</b></h1>
<h1 style="color:#dd5e3b;"><b>Cat�gorie Entreprises Confirm�es Bruxelloises</b></h1>
<!-- --------------------------------  ECB  ---------------------------------------------- -->


<br/>
<?php
$nb = 5;
$sql = "select * from fiches where categorie='ecb' ORDER BY nom";
	$rs = mysql_query($sql) or die(mysql_error());
	echo'<table id="table_liste">';
	$i = 1;
	while($row = mysql_fetch_array($rs)){
		$id = $row["id"];
		$titre = stripslashes($row['nom']);
		$logo = $row["logo"];
?>

<?php
		if($i == 1) { echo'<tr>'; }
		echo'<td valign="top">
		<div id="result">
		<a class="fancybox fancybox.ajax"  href="affiche_entreprise.php?id='.$id.'">
		<img border="0" width="180" src="uploads/'.$logo.'" />
		</a> '.$titre.'<br/>
		</div>
		<div id="titre"><br/><a class="fancybox fancybox.ajax" id="btn_vote2" target="_blank" href="affiche_entreprise.php?id='.$id.'">FICHE</a> <a style="display:none;" id="btn_vote" target="_blank" href="vote_ecb.php?id='.$id.'">&rarr; VOTER</a></div></td>';
		$i++;
		if($i > $nb) { echo'</tr>'; $i = 1; }
		
?>

<?php
	}
	echo'</table>';
?>
<br/>
<a target="_blank" href="liste_entreprise_ecw.php">&rarr; Voir les entreprises de la cat&eacute;gorie 'Entreprises Confirm&eacute;es Wallonnes'</a>
<!--
<p><br/><br/>
Comme indiqu� dans le <a href="http://www.prixdeleconomiesociale.be/wp-content/uploads/2012/04/regl_ENTREPRISE.pdf" target="_blank">r�glement du prix entreprise</a>, <b>le score final des votes ne d�termine pas les laur�ats et gagnants du prix</b>, ils permettent uniquement d�acc�der au deuxi�me jury (le jury pl�nier). L�objectif des votes vise essentiellement la promotion du secteur de l�Economie Sociale, du prix de l�Economie Sociale et des structures participantes.
Un seul vote par adresse mail sera accept�. <br/><br/>
Les organisateurs se r�servent le droit d�exclure un candidat en cas d�abus ou de tricherie.
</p>    -->

</div>
<div id="footer-bottom">
<?php
	include("footer.php");
?>
</div>

<br /><br />

</body>
</html>
