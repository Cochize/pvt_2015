<html>
<head>
<style>
html{
	font-family: arial;
	font-size: 14px;
	color:#333;
	margin: 50px;
	line-height: 18px;
}
ol{
	margin : 0 20px 0 20px;
}
li{
	padding: 10px 0 10px 0;
	border-bottom: 1px dotted #ccc;
}
a{
	text-decoration:none;
	color: #d44c00;
	font-weight:bold;
}
a:hover{
	text-decoration:none;
	color: #36d2ab;
	font-weight:bold;
}
h2{
	font-size: 16px;
	padding:0;
	margin:30px 0 10px 0;
}
</style>
</head>
<body>
	<h1>Prix de l&rsquo;&Eacute;conomie Sociale 2014 | Jury Prix Edition</h1>
	<ol>

	<?php
		$dirname = './';
		$dir = opendir($dirname); 
		$i = 0;
		$liste_rep = scandir("./");
		$num = count($liste_rep);
												
		while(($file = readdir($dir)) && ($i < $num)) {
			$i++;
			if(($liste_rep[$i]) != '.' && ($liste_rep[$i]) != '..' && ($liste_rep[$i]) != 'Thumbs.db'  && ($liste_rep[$i]) != 'index.php'  && ($liste_rep[$i]) != '.htaccess'  && ($liste_rep[$i]) != '.htpasswd' && !is_dir($dirname.$liste_rep[$i]))
			{									

				echo '<li><a target="_blank" href="'.$dirname.$liste_rep[$i].'">'.$liste_rep[$i].'</a>'.'</li>';
				
			}
		}

		closedir($dir);
												
	?>
	</ol>
	<h2>M�moires �ligibles</h2>
	<ol>

	<?php
		$dirname2 = './ELIGIBLE/';
		$dir2 = opendir($dirname2); 
		$p = 0;
		$liste_rep2 = scandir("./ELIGIBLE/");
		$num2 = count($liste_rep2);
												
		while(($file2 = readdir($dir2)) && ($p < $num2)) {
			$p++;
			if(($liste_rep2[$p]) != '.' && ($liste_rep2[$p]) != '..' && ($liste_rep2[$p]) != 'Thumbs.db'  && ($liste_rep2[$p]) != 'index.php' && !is_dir($dirname2.$liste_rep2[$p]))
			{									

				echo '<li><a target="_blank" href="'.$dirname2.$liste_rep3[$p].'">'.$liste_rep2[$p].'</a>'.'</li>';
				
			}
		}

		closedir($dir2);
												
	?>
	</ol>
	<h2>M�moires non �ligibles</h2>
	<ol>

	<?php
		$dirname3 = './NON_ELIGIBLE/';
		$dir3 = opendir($dirname3); 
		$k = 0;
		$liste_rep3 = scandir("./NON_ELIGIBLE/");
		$num3 = count($liste_rep3);
												
		while(($file3 = readdir($dir3)) && ($k < $num3)) {
			$k++;
			if(($liste_rep3[$k]) != '.' && ($liste_rep3[$k]) != '..' && ($liste_rep3[$k]) != 'Thumbs.db'  && ($liste_rep3[$k]) != 'index.php' && !is_dir($dirname3.$liste_rep3[$k]))
			{									

				echo '<li><a target="_blank" href="'.$dirname3.$liste_rep3[$k].'">'.$liste_rep3[$k].'</a>'.'</li>';
				
			}
		}

		closedir($dir3);
												
	?>
	</ol>
