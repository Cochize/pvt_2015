<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Prix de l'Economie Sociale 2015 - Fiche de pr&eacute;sentation</title>

<script type="text/javascript" src="js/jquery-1.3.2.js" ></script>
<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="les.css" />
<link href='http://fonts.googleapis.com/css?family=Actor' rel='stylesheet' type='text/css'>
<script type="text/javascript" >
	$(function(){
		var btnUpload=$('#upload');
		var status=$('#status');
		new AjaxUpload(btnUpload, {
			action: 'upload-file.php',
			name: 'uploadfile',
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // type de fichier non admis!
					status.html('<br/><b style="color:red;">ECHEC! Seuls les fichiers JPG, PNG ou GIF sont autoris&eacute;s.</b>');
					return false;
				}
				status.html('<br/><b style="color:green;">Merci de patienter quelques instants pendant l&rsquo;upload du logo...</b>');
			},
			onComplete: function(file, response){
				//On completion clear the status
				status.text('');
				//Add uploaded file to list
				if(response==="success"){
					$('<li></li>').appendTo('#files').html('<img src="./uploads/'+file+'" alt="" /><input type="text" name="logo" value='+file+'><br />'+file).addClass('success');

					
				} else{
					$('<li></li>').appendTo('#files').text(file).addClass('error');
				}
			}
		});
		
	});
</script>


</head>

<body>
<?php
include("my_connection.inc.php");

date_default_timezone_set('Europe/Brussels');
$today = date("YmdHis_");

?>
<div id="container">

<?php
$transmi = "";
if(isset($_POST['envoi2'])){
			

			
		 if($_POST['from_txt'] != "" && $_POST['orga_txt'] != "" && $_POST['adresse_txt'] != ""){
		 
		 
 		// v&eacute;rif de l'e-mail
 		$from = $_POST['from_txt'];
 
 		$atom   = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';   // caractres autoris&eacute;s avant l'arobase
		$domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)'; // caractres autoris&eacute;s aprs l'arobase (nom de domaine)
                               
		$regex = '/^' . $atom . '+' .   // Une ou plusieurs fois les caractres autoris&eacute;s avant l'arobase
		'(\.' . $atom . '+)*' .         // Suivis par z&eacute;ro point ou plus
										// s&eacute;par&eacute;s par des caractres autoris&eacute;s avant l'arobase
		'@' .                           // Suivis d'un arobase
		'(' . $domain . '{1,63}\.)+' .  // Suivis par 1  63 caractres autoris&eacute;s pour le nom de domaine
										// s&eacute;par&eacute;s par des points
		$domain . '{2,63}$/i';          // Suivi de 2  63 caractres autoris&eacute;s pour le nom de domaine

		// test de l'adresse e-mail
		if (preg_match($regex, $from)) {
		//script de r&eacute;ception des donn&eacute;s, d'envoi par mail
			
			 $nom = addslashes($_POST['orga_txt']);
			 $adresse = addslashes($_POST['adresse_txt']);
			 $tel = addslashes($_POST['tel_txt']);
			 $logo = addslashes($_POST['logo']);
			 $logo_rename = $today.$logo;
			 $web = $_POST['web_txt'];
			 $presentation = addslashes($_POST['presentation_txt']);
			 $motivation = addslashes($_POST['motivation_txt']);
   


			 
			 $to = "info@prixdeleconomiesociale.be";
			 $subject = "[prixdeleconomiesociale.be] - R�ception de votre fiche de pr�sentation";
			 $msg_final = "<table border='0'>
			 				<tr><td>
							De : Prix de l&rsquo;&Eacute;conomie sociale<br>
							Sujet : R�ception de votre fiche de pr�sentation<br>
							<br><br>
							Bonjour,
							<br><br>
							Nous avons r&eacute;ceptionn&eacute; votre fiche de pr�sentation. Merci!<br>
							Afin que votre candidature soit compl&egrave;te, n&rsquo;oubliez pas de nous faire parvenir votre formulaire de candidature et le fichier d&rsquo;analyse synth&eacute;tique. Ceux-ci sont disponibles � l&rsquo;adresse : www.prixdeleconomiesociale.be/prix-entreprise
							<br><br>
							Les organisateurs du Prix de l&rsquo;&Eacute;conomie sociale
							<br><br>
							--------------------------------<br>
							(http://www.prixdeleconomiesociale.be)<br><br>
							</table>";
			 
			 $headers = 'From: info@prixdeleconomiesociale.be' . "\r\n" .
			 'Reply-To: info@prixdeleconomiesociale.be' . "\r\n" .
			 'X-Mailer: PHP/' . phpversion();
			 
			 $headers .= "MIME-version: 1.0\n";
			 $headers .= "Content-type: text/html; charset= iso-8859-1\n";
			 $headers .= "Cc: " . $from . "\n";
			 mail($to,$subject,$msg_final, $headers);
			 
						 
$sql= "INSERT INTO fiches (nom, email, adresse, logo, tel, web, presentation, motivation)
		                 values ('$nom', '$from', '$adresse', '$logo_rename', '$tel', '$web', '$presentation', '$motivation')";
						 
	mysql_query($sql) or die (mysql_error());
			
					$transmi2="<span style='color:#3d9b14; font-size:14px; background-color: #cce9bf; border: 1px solid #3d9b14; padding: 3px;''>Merci ! Votre fiche de pr&eacute;sentation a &eacute;t&eacute; enregistr&eacute;e.</span>";
				

		
		} else {
		
					$transmi2="<span style='color:#a01916; font-size:14px; background-color: #f8a3a3; border: 1px solid #ed1b1b; padding: 3px;'>ECHEC : Veuillez entrer une adresse e-mail VALIDE</span>";
				
				
			}
			
		}else{
			
					$transmi2="<span style='color:#a01916; font-size:14px; background-color: #f8a3a3; border: 1px solid #ed1b1b; padding: 3px;'>ECHEC : Veuillez remplir tous les champs du formulaire</span>";
				
		
		}
}


?>
<div class="titre_page"><img src="logo.jpg" alt="Prix de l'Economie Sociale" width="400" /></div>


<h2>Candidature aux prix entreprises : fiche de pr&eacute;sentation</h2>
<p style=' border-bottom: 1px solid #e0e0e0; font-size: 14px; line-height:22px; margin: 20px 20px 20px 0; padding: 0 0 20px 0;'>
<b><?php echo $transmi2; ?></b>
Afin de r&eacute;aliser une pr&eacute;sentation optimale sur notre site de votre structure et de <b>susciter les votes en ligne</b>, merci de remplir correctement le formulaire ci-dessous :
</p>



<form name="contact_frm" action="index.php" method="POST">
		<table valign="top">			

					<tr>
						<td valign="top"><label for="orga">Nom de votre structure</label></td>
						<td colspan="2"><input type="text" id="orga" name="orga_txt" size="60" /></td>
					</tr>
					<tr>
						<td valign="top"><label for="orga">Logo de votre structure</label>

						</td>
						<td colspan="2">
							<div id="mainbody" >
								<!-- Upload Button, use any id you wish-->
								<div id="upload" ><span>Cliquez ici pour t&eacute;l&eacute;charger votre logo<br>(formats JPG ou PNG uniquement)<span></div><span id="status" ></span>
								<ul id="files" ></ul>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top"><label for="adresse">Adresse</label></td>
						<td colspan="2"><input type="text" id="adresse" name="adresse_txt" size="60" /></td>
					</tr>
					<tr>
						<td valign="top"><label for="tel">T&eacute;l&eacute;phone / Gsm</label></td>
						<td colspan="2"><input type="text" id="tel" name="tel_txt" size="60" /></td>
					</tr>
					<tr>
						<td valign="top"><label for="mail">Adresse e-mail</label></td>
						<td colspan="2"><input type="text" id="mail" name="from_txt" size="60" /></td>
					</tr><br/>
					<tr>
						<td valign="top"><label for="web">Site web</label></td>
						<td colspan="2"><input type="text" id="web" name="web_txt" size="60" /></td>
					</tr>
					<tr>
						<td colspan="3"><br/><br/><label for="nom">Pr&eacute;sentation de votre structure</label>
						<br/><br/><textarea id="nom" name="presentation_txt" maxlength="1000" rows=20 COLS=70 />1000 caract&egrave;res MAXIMUM (espaces compris)</textarea></td>
					</tr>
<tr>
						<td colspan="3"><br/><label for="nom">Affectation de la bourse</label>
					<br/><br/><textarea id="nom" name="motivation_txt" maxlength="1000" rows=20 COLS=70 />1000 caract&egrave;res MAXIMUM (espaces compris)</textarea></td>
					</tr>
					<tr>
							<!--<td colspan="2"><div id="button_clot">INSCRIPTIONS CL&Ocirc;TUR&Eacute;ES</div></td>-->
						<td colspan="2"><input type="submit" id="button" name="envoi2" value="Inscription" /></td>
					</tr>
</form>
</table>

<p><br/><span class="obl">* Tous les champs sont obligatoires</span></p>
<p style=' border-top: 1px solid #e0e0e0; font-size: 14px; line-height:22px; margin: 20px 0 0 0; padding: 20px 0 0 0;'>
	Prix Roger Vanthournout asbl
	<br/><br/>
	Rue de Steppes, 24 - 4000 Li&egrave;ge<br/>
	Contact : <a href="mailto:info@prixdeleconomiesociale.be">Anne-France Paligot</a> - 04 227 58 89<br/>
	<a href="http://www.prixdeleconomiesociale.be" target="_blank">www.prixdeleconomiesociale.be &rarr;</a>
</p>
</div>


</body>
</html>
