<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Remise du Prix de l'Economie Sociale - Inscriptions 2013</title>


<style type="text/css">
body{
	font-family: Arial;
	font-size: 12px;
	color: #0c3c54;
	margin: 0px; 
	background:#eee;
}

#container{
	width: 600px;
	margin: 0 auto;
	background:#fff;
	padding: 50px;
}
a{
	color:#e75229;
	text-decoration:none;
}
label{
	margin-right: 20px;
}

#button_clot{
	background-color: #CCC;
	color:#000;
	border: 0px;
	padding: 2px;
	font-family: verdana;
	font-size: 14px;
	margin-top: 20px;
	cursor: pointer;
	width:155px;
}
#button{
	background-color: #e8552b;
	color: white;
	border: 0px;
	padding: 8px 14px;
	font-family: verdana;
	font-size: 12px;
	margin-top: 20px;
	cursor: pointer;
	font-weight:bold;
	text-transform:uppercase;
}
#button:hover{
	background-color: #8EBB1E;
	color: white;
	border: 0px;
	padding: 8px 14px;
	font-family: verdana;
	font-size: 12px;
	margin-top: 20px;
	cursor: pointer;
	font-weight:bold;
	text-transform:uppercase;
}
.programme{
	font-size: 14px;
	margin:0;
	padding:0;
	text-align:justify;
	background-color: #efefef;
}
table input, table select{
	border:none;
	background:#eee;
	padding: 5px 10px;
}
table {
	font-size:14px;
}
table label{
	padding: 20px 0;
	diplay:inline-block;
}
.obl{
	font-size: 12px;
	color: #e8552b;
}
</style>

</head>

<body>
<?php
include("my_connection.inc.php");
?>
<div id="container">

<?php
$transmi = "";
if(isset($_POST['envoi2'])){
			

			
		 if($_POST['from_txt'] != "" && $_POST['nom_txt'] != "" && $_POST['orga_txt'] != "" && $_POST['adresse_txt'] != "" && $_POST['cerem_txt'] != "" && $_POST['coc_txt'] != ""){
		 
		 
 		// v&eacute;rif de l'e-mail
 		$from = $_POST['from_txt'];
 
 		$atom   = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';   // caractres autoris&eacute;s avant l'arobase
		$domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)'; // caractres autoris&eacute;s aprs l'arobase (nom de domaine)
                               
		$regex = '/^' . $atom . '+' .   // Une ou plusieurs fois les caractres autoris&eacute;s avant l'arobase
		'(\.' . $atom . '+)*' .         // Suivis par z&eacute;ro point ou plus
										// s&eacute;par&eacute;s par des caractres autoris&eacute;s avant l'arobase
		'@' .                           // Suivis d'un arobase
		'(' . $domain . '{1,63}\.)+' .  // Suivis par 1  63 caractres autoris&eacute;s pour le nom de domaine
										// s&eacute;par&eacute;s par des points
		$domain . '{2,63}$/i';          // Suivi de 2  63 caractres autoris&eacute;s pour le nom de domaine

		// test de l'adresse e-mail
		if (preg_match($regex, $from)) {
		//script de r&eacute;ception des donn&eacute;s, d'envoi par mail
			
			 $nom = $_POST['nom_txt'];
			 $orga = $_POST['orga_txt'];
			 $adresse = $_POST['adresse_txt'];
			 $cerem = $_POST['cerem_txt'];
			 $coc = $_POST['coc_txt'];
			 
			 $nom2 = stripslashes($nom);
			 $orga2 = stripslashes($orga);
			 $adresse2 = stripslashes($adresse);
			 
			 
			 $to = "info@prixdeleconomiesociale.be, informatique@ages.be";
			 
			 $subject = "[prixdeleconomiesociale.be] - Confirmation de votre inscription";
			 $msg_final = "<table border='0'>
			 				<tr><td>Merci! Votre inscription &agrave; la c&eacute;r&eacute;monie de remise des prix a &eacute;t&eacute; enregistr&eacute;e.<br><br>
							La c&eacute;r&eacute;monie se d&eacute;roulera le mardi 2 d&eacute;cembre 2014 &agrave; la Tricoterie, &agrave; Bruxelles : <br/>Rue Th&eacute;odore Verhaegen 158 B-1060 Saint-Gilles.<br><br>
			 				
							D&eacute;tail de votre inscription :<br></td></tr>
							<tr><td>M - Mme: </td><td>$nom2</td></tr>
							<tr><td>Organisation : </td><td>$orga2</td></tr>
							<tr><td>Adresse : </td><td>$adresse2</td></tr>
							<tr><td>E-mail : </td><td>$from</td></tr>
							<tr><td>Participation &agrave; la c&eacute;r&eacute;monie : </td><td><b>$cerem</b></td></tr>
							<tr><td>Participation au cocktail : </td><td><b>$coc</b></td></tr>
							</table>";
			 
			 $headers = 'From: ' . $to . "\r\n" .
			 'Reply-To: ' . $to . "\r\n" .
			 'X-Mailer: PHP/' . phpversion();
			 $headers .= "MIME-version: 1.0\n";
			 $headers .= "Content-type: text/html; charset= iso-8859-1\n";
			 $headers .= "Cc: " . $from . "\n";
			 mail($to,$subject,$msg_final, $headers);
			 
$sql= "INSERT INTO inscription (nom, orga, mail, adresse, cerem, coc)
		                 values ('$nom', '$orga', '$from', '$adresse', '$cerem', '$coc')";
	mysql_query($sql) or die (mysql_error());
			
					$transmi2="<span style='color:#3d9b14; font-size:14px; background-color: #cce9bf; border: 1px solid #3d9b14; padding: 8px 14px;''>Merci ! Votre inscription a &eacute;t&eacute; enregistr&eacute;e.</span><br/><br/>";
				
			
		
		} else {
		
					$transmi2="<span style='color:#a01916; font-size:14px; background-color: #f8a3a3; border: 1px solid #ed1b1b; padding: 8px 14px;'>ECHEC : Veuillez entrer une adresse e-mail VALIDE</span><br/><br/>";
				
				
			}
			
		}else{
			
					$transmi2="<span style='color:#a01916; font-size:14px; background-color: #f8a3a3; border: 1px solid #ed1b1b; padding: 8px 14px;'>ECHEC : Veuillez remplir tous les champs du formulaire</span><br/><br/>";
				
		
		}
}


?>
<div class="titre_page"><img src="logo.jpg" alt="PVT" /></div>


<p style='font-size:20px; line-height: 30px;'>
<b><?php echo $transmi2; ?></b>
<b>La c&eacute;r&eacute;mone de remise des Prix de l'&Eacute;conomie Sociale<br/>se d&eacute;roulera 
le mardi 2 d&eacute;cembre 2014<br/>&agrave; la <a href="http://www.tricoterie.be/-Contact-?lang=fr" target="_blank">TRICOTERIE, &agrave; Bruxelles</a>.</b></p>

<p style='font-size:14px; line-height: 22px;'><b>L&rsquo;inscription &agrave; cet &eacute;v&egrave;nement est obligatoire.<br/>Merci de compl&eacute;ter le formulaire ci-dessous avant le 27 novembre 2014.</b></p>


<table>
<form name="contact_frm" action="index.php" method="POST">
					
					<tr>
						<td><label for="nom">M - Mme <span class="obl">*</span></label></td>
						<td colspan="2"><input type="text" id="nom" name="nom_txt" size="60" /></td>
					</tr>
					<tr>
						<td><label for="orga">Organisation <span class="obl">*</span></label></td>
						<td colspan="2"><input type="text" id="orga" name="orga_txt" size="60" /></td>
					</tr>
					
					<tr>
						<td><label for="adresse">Adresse <span class="obl">*</span></label></td>
						<td colspan="2"><input type="text" id="adresse" name="adresse_txt" size="60" /></td>
					</tr>
					
					<tr>
						<td><label for="mail">E-mail <span class="obl">*</span></label></td>
						<td colspan="2"><input type="text" id="mail" name="from_txt" size="60" /></td>
					</tr>
					
					<tr>
						<td></td>
						<td><br/><label for="cerem">Je participerai &agrave; la c&eacute;r&eacute;monie &agrave; 11h <span class="obl">*</span></label></td>
						<td><br/><select id="cerem" name="cerem_txt">
							<option> --- </option>
							<option>oui</option>
							<option>non</option>
							</select></td>
					</tr>
					
					<tr>
						<td></td>
						<td><label for="coc">Je participerai au cocktail d&icirc;natoire &agrave; 12h30 <span class="obl">*</span></label></td>
						<td><select id="coc" name="coc_txt">
							<option> --- </option>
							<option>oui</option>
							<option>non</option>
							</select></td>
					</tr>
					
					
					<tr>
						<td></td>
                        <!--<td colspan="2"><div id="button_clot">INSCRIPTIONS CL&Ocirc;TUR&Eacute;ES</div></td>-->
						<td colspan="2"><input type="submit" id="button" name="envoi2" value="Inscription" />
						</td>
					</tr>
</form>
</table>

<p><br/><span class="obl">* Tous les champs sont obligatoires</span></p>

<p style=' border-top: 1px solid #e0e0e0; font-size: 14px; line-height:22px; margin: 20px 0 0 0; padding: 20px 0 0 0;'>
Prix Roger Vanthournout asbl
<br/><br/>
Rue de Steppes, 24 - 4000 Li&egrave;ge<br/>
Contact : <a href="mailto:info@prixdeleconomiesociale.be">Anne-France Paligot</a> - 04 227 58 89<br/>
<a href="http://www.prixdeleconomiesociale.be" target="_blank">www.prixdeleconomiesociale.be &rarr;</a></p>
</div>

</body>
</html>
